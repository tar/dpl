#!/bin/bash

if [ -z "$1" ]; then
   echo "Usage: $0 uid|email"
   exit 1
fi

case "$1" in
	*@*)
		lookup=email
		;;
	*)
		lookup=uid
		;;
esac

json_output=`curl -s "https://nm.debian.org/api/people?$lookup=$1" | jshon -e r -e 0`
status_changed=`LC_ALL=C date -u -d@$(echo $json_output | jshon -e status_changed -u) +"%B %d, %Y"`

status=`echo $json_output | jshon -e status -u | sed "s/_//"`
case "$status" in
	ddu|ddnu|dm)
		;;
	*)
		echo "E: $1 is not a DD or DM yet, according to nm.debian.org"
		exit 102
		;;
esac

fullname=`echo $json_output | jshon -e fullname -u`

# Pretty-print the user's fingerprint
fingerprint=`echo $json_output | jshon -e fpr -u | sed 's/.\{4\}/&/g'`

# Generate a certificate for $1
sed -e "s/LOGIN/$1/g"                     \
    -e "s/FINGERPRINT/$fingerprint/g"     \
    -e "s/FULLNAME/$fullname/g"           \
    -e "s/CREATIONDATE/$status_changed/g" \
    -e "s/STATUS/$status/g"               \
    dd-certificate.tex                    \
  > $1.tex
